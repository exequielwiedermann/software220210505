import 'dart:html';

import 'package:flutter/material.dart';

import 'package:flutter/material.dart';
import 'dart:async';
import '';

class SplashScreen extends StatefulWidget{
  @override
  _SplashScreenState createState() = _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>{
  Timer _timer;

  @override
  void initState(){
    super.initState();
    _timer = Timer(const Duration(seconds: 2), _onShowLogin);
  }

  @override
  void dispose(){
    _timer.cancel();
    super.dispose();
  }

  void _onShowLogin{
    if(mounted){
      Navigator.of(context).pushReplacement(LoginScreen.route());
    }
  }

//child: condition == true ? new Container(): new Container()

//new Center(
//child:
//if(condition == true){
//new child();
//}else{
//new Container();
//}
//)

//for(var i = 0; i<2; i++){
//print(i);
//}

//Patrón iterador que se llama Iterator (Listas o Array, Cola o Pila)
//Array = Espacio de memoria (0 hasta donde termina)
//Array de 3 posiciones 0 a 2, 0 1 2
//Las posiciones en un array son igual a espacio de memoria, y ahi es donde 
//guardo información en cada posición yo guardo Nicolás
//Cola = a la cola del banco. Primero que entra primero que sale FIFO First Int First Out
//Pila = último primero que sale, LIFO Last Int First Out
//var callbacks = []
//callbacks.forEach((c)=>c());



  @override
  Widget build(BuildContext context){
    return Material(
      color: Colors.grey[600],
      child: Column(
        children: <Widget> [
          SizedBox(height: 100.0,),
          Flexible(
            flex: 2,
            child: SafeArea(
              child: FractionallySizedBox(
                widthFactor: 0.7,
                //Acá se va a poner la foto
                //child: Image.asset('assets/foto.jpg'),
              ),
            ),
          ),
          Text(
            'Bienvenidos a la app',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 18.0,
            ),
          ),
          Flexible(
            flex: 2,
            child: SafeArea(
              child: Container(
                padding: const EdgeInsets.symmetric(vertical: 64.0, horizontal: 16.0),
                alignment: Alignment.bottomCenter,
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}